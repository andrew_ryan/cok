pub use cli::Cli;
#[allow(warnings)]
pub mod cli {
    pub fn print_help(content:impl ToString){
        let content = content.to_string().clone();
        let json = json::parse(&content).expect("parse json error");
        // println!("{}", json);
        let name = &json["name"]
            .as_str()
            .expect("parse name in json error")
            .to_string();
        let version = &json["version"].as_str().unwrap_or(" ").to_string();
        let authors = &json["authors"]
            .members()
            .into_iter()
            .map(|a| a.as_str().unwrap_or(" ").to_string())
            .collect::<Vec<_>>();

        let description = &json["description"].as_str().unwrap_or(" ").to_string();
        let options = &json["options"]
            .members()
            .into_iter()
            .map(|s| {
                let arg = s["name"].as_str().unwrap_or(" ").to_string();
                let short = s["short"].as_str().unwrap_or(" ").to_string();

                let description = s["description"].as_str().unwrap_or(" ").to_string();
                (arg, short, description)
            })
            .collect::<Vec<_>>();

        let commands = &json["commands"]
            .members()
            .into_iter()
            .map(|s| {
                let arg = s["name"].as_str().unwrap_or(" ").to_string();
                let short = s["short"].as_str().unwrap_or(" ").to_string();

                let description = s["description"].as_str().unwrap_or(" ").to_string();

                (arg, short, description)
            })
            .collect::<Vec<_>>();
        println!("{} {}", name, version);
        println!("{}", authors.join(","));
        println!("{}", description);
        println!("");
        println!("Usage: {} [OPTIONS] [COMMAND]\n", name);
        if options.len() > 0 {
            println!("Options:");
            options.iter().for_each(|s| {
                if s.1.to_string() == " " {
                    println!("{}\t{}\t\t{}", name, s.0.to_string(), s.2);
                } else if s.0.to_string() == " " {
                    println!("{}\t{}\t\t{}", name, s.1.to_string(), s.2);
                } else {
                    println!(
                        "{}\t{:^}, {}\t{}",
                        name,
                        s.0.to_string(),
                        s.1.to_string(),
                        s.2
                    );
                }
            });
            println!("");
        }
        if commands.len() > 0 {
            println!("Commands:");
            commands.iter().for_each(|s| {
                if s.1.to_string() == " " {
                    println!("{}\t{}\t\t{}", name, s.0.to_string(), s.2);
                } else if s.0.to_string() == " " {
                    println!("{}\t{}\t\t{}", name, s.1.to_string(), s.2);
                } else {
                    println!(
                        "{}\t{:^}, {}\t{}",
                        name,
                        s.0.to_string(),
                        s.1.to_string(),
                        s.2
                    );
                }
            });
        }
        println!("");
        println!("{}\t-h --help\tPrints help information", name);
    }
    pub fn create_demo_json(){
        let contents = include_str!("../demo.json");
        std::fs::write("cli.json", contents).unwrap();
    }
    #[derive(Debug, Clone, Default)]
    pub struct Cli{
        json:String,
    }
    impl Cli {
        ///# load cli info from json
        /// ```rust
        /// fn main() {
        /// use cok::cli::Cli;
        /// let cli = Cli::load_from_json(r#"
        /// {
        ///     "name":"cargo",
        ///     "version":"1.0.0",
        ///     "authors":["andrew <dnrops@anonymous.com>","ryan <rysn@gmail.com>"],
        ///     "description":"Rust's package manager",
        ///     "options":[
        ///         {
        ///             "name":"--version",
        ///             "short":"V",
        ///             "description":"Print version info and exit"
        ///         }
        ///     ],
        ///     "commands":[
        ///         {
        ///                 "name":"build",
        ///                 "short":"b",
        ///                 "description":"Compile the current package"
        ///         }
        ///     ]
        /// }
        /// "#);
        ///}
        /// ```
        ///
        pub fn new(json:impl ToString) -> Self {
            let args = doe::args!();
            if args.len() < 1
                || (args.len() == 1 && args[0] == "-h")
                || (args.len() == 1 && args[0] == "--help")
            {
                print_help(json.to_string());
            }
            Self{json:json.to_string()}
        }
      
        pub fn print_help(&self){
            let content = self.json.clone();
            let json = json::parse(&content).expect("parse json error");
            // println!("{}", json);
            let name = &json["name"]
                .as_str()
                .expect("parse name in json error")
                .to_string();
            let version = &json["version"].as_str().unwrap_or(" ").to_string();
            let authors = &json["authors"]
                .members()
                .into_iter()
                .map(|a| a.as_str().unwrap_or(" ").to_string())
                .collect::<Vec<_>>();
    
            let description = &json["description"].as_str().unwrap_or(" ").to_string();
            let options = &json["options"]
                .members()
                .into_iter()
                .map(|s| {
                    let arg = s["name"].as_str().unwrap_or(" ").to_string();
                    let short = s["short"].as_str().unwrap_or(" ").to_string();
    
                    let description = s["description"].as_str().unwrap_or(" ").to_string();
                    (arg, short, description)
                })
                .collect::<Vec<_>>();
    
            let commands = &json["commands"]
                .members()
                .into_iter()
                .map(|s| {
                    let arg = s["name"].as_str().unwrap_or(" ").to_string();
                    let short = s["short"].as_str().unwrap_or(" ").to_string();
    
                    let description = s["description"].as_str().unwrap_or(" ").to_string();
    
                    (arg, short, description)
                })
                .collect::<Vec<_>>();
            println!("{} {}", name, version);
            println!("{}", authors.join(","));
            println!("{}", description);
            println!("");
            println!("Usage: {} [OPTIONS] [COMMAND]\n", name);
            if options.len() > 0 {
                println!("Options:");
                options.iter().for_each(|s| {
                    if s.1.to_string() == " " {
                        println!("{}\t{}\t\t{}", name, s.0.to_string(), s.2);
                    } else if s.0.to_string() == " " {
                        println!("{}\t{}\t\t{}", name, s.1.to_string(), s.2);
                    } else {
                        println!(
                            "{}\t{:^}, {}\t{}",
                            name,
                            s.0.to_string(),
                            s.1.to_string(),
                            s.2
                        );
                    }
                });
                println!("");
            }
            if commands.len() > 0 {
                println!("Commands:");
                commands.iter().for_each(|s| {
                    if s.1.to_string() == " " {
                        println!("{}\t{}\t\t{}", name, s.0.to_string(), s.2);
                    } else if s.0.to_string() == " " {
                        println!("{}\t{}\t\t{}", name, s.1.to_string(), s.2);
                    } else {
                        println!(
                            "{}\t{:^}, {}\t{}",
                            name,
                            s.0.to_string(),
                            s.1.to_string(),
                            s.2
                        );
                    }
                });
            }
            println!("");
            println!("{}\t-h --help\tPrints help information", name);
        }
       
        pub fn create_demo_json(&self){
            let contents = include_str!("../demo.json");
            std::fs::write("cli.json", contents).unwrap();
        }
        pub fn args(&self) -> Vec<String> {
            doe::args!()
        }
    }
}
