#[allow(warnings)]
fn main() {
    use cok::Cli;
    let json = include_str!("../cli.json");
    let cli = Cli::new(json);
    let args = cli.args();
    if args.len() !=3{
        cli.print_help()
    }
}
