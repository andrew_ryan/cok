# A simple to use, efficient, Command Line Argument Parser
[![Crates.io](https://img.shields.io/crates/v/cok.svg)](https://crates.io/crates/cok)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/cok)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/cok/-/raw/master/LICENSE)


## Example
create cli.json
```json
{
    "name":"cargo",
    "version":"1.0.0",
    "authors":["andrew <dnrops@anonymous.com>","ryan <rysn@gmail.com>"],
    "description":"Rust's package manager",
    "options":[
        {
            "name":"--version",
            "short":"V",
            "description":"Print version info and exit"
        },
        {
            "name":"--offline",
            "description":"Run without accessing the network"
        }
    ],
    "commands":[
        {
            "name":"build",
            "short":"b",
            "description":"Compile the current package"
        },
        {
            "name":"check",
            "short":"c",
            "description":"Analyze the current package and report errors, but don't build object files"
        },
        {
            "name":"clean",
            "description":"Remove the target directory"
        },
        {
            "name":"new",
            "description":"Create a new cargo package"
        },
        {
            "name":"add",
            "description":"Add dependencies to a manifest file"
        }
    ]
}
```
```
cargo add cok
```
main.rs
```rust
fn main() {
    use cok::Cli;
    let json = include_str!("../cli.json");
    let cli = Cli::new(json);
    let args = cli.args();
    if args.len() !=3{
        cli.print_help()
    }
}

```
a simple cli
```
cargo 1.0.0
andrew <dnrops@anonymous.com>,ryan <rysn@gmail.com>
Rust's package manager

Usage: cargo [OPTIONS] [COMMAND]

Options:
cargo   build, b        Compile the current package
cargo   check, c        Analyze the current package and report errors, but don't build object files
cargo   clean           Remove the target directory
cargo   new             Create a new cargo package
cargo   add             Add dependencies to a manifest file

Commands:
cargo   build, b        Compile the current package
cargo   check, c        Analyze the current package and report errors, but don't build object files
cargo   clean           Remove the target directory
cargo   new             Create a new cargo package
cargo   add             Add dependencies to a manifest file
cargo   -h --help       Prints help information
```